"""Logic Tensor Network for PASCAL-part dataset"""

import os
import time
import itertools

from src.log import Log
import src.config as prg

import torch.optim as optim
import torch
import numpy as np

def save_model(model, model_label, optimizer, i):
    import src.checkpoint_manager as checkpoint_manager
    # Save model to logdir with default filename
    save_model_path = './data/trial/last/checkpoints/' # running with jupyter so can't pass args
    if save_model_path != '':
        log.debug('=====Saving model parameters=====')
        # Save model and optimizer parameters. This is good for further training
        save_args = {
                'model': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'epoch': i  # Not really needed but perhaps useful to know
                }
        checkpoint_manager.save_checkpoint(save_args, basepath=save_model_path,
                filename='checkpoint{}_{}.pth.tar'.format(model_label, i))
        # Problem is that LTN is complex to initialise, as we need the clause schema.
        # Therefore, try saving the whole model (structure and all)
        checkpoint_manager.save_checkpoint(
                {'full_model': model},
                basepath=save_model_path,
                filename='checkpoint_full{}_{}.pth.tar'.format(model_label, i),
                full_model=True)

def run(log, for_data='pascal_part', save_model_path='', load_model_path=''):
    #if True in prg.INCLUDE_CONSTRAINTS and prg.DATA_SUBSET is not None:

        #raise Exception('Ontology is included but using data subset. Breaking.')
    gitsave_success = Log.save_git_version()
    if not gitsave_success:
        log.warning('=Warning - git version was not saved successfully=')
    #####
    # Note, using Lazy import - need logger to be setup before, which is done when the program
    # ran (__name__ == '__main__') below
    ####
    # DM ~ DataManager
    if for_data == 'pascal_part':
        import src.pascal_part as dm
    #####

    log.info('======Experiment Start======')
    log.info('=====INITIALISATION Phase=====')
    # Make sure save and load refer to the log directory that is setup already
    if save_model_path != '':
        save_model_path = os.path.join(Log.log_dir, save_model_path)
    if load_model_path != '':
        load_model_path = os.path.join(Log.log_dir, load_model_path)

    ### Load Data
    # Extract a dictionary with data pulled from associated files
    # Key: ...
    log.info('====Loading data from files and creating datasets====')
    experiment_dsets, ontology_dset, ref_dsets = dm.initialise_train_datasets(
            prg.THRESHOLD_SIZE, prg.DATA_SUBSET, (True in prg.INCLUDE_CONSTRAINTS))
    # review
    #parts_for_whole_dict = experiment_data['parts_for_whole_dict']
    #wholes_for_part_dict = experiment_data['wholes_for_part_dict']

    # If running tests, only load/sample the test data once
    """
    if prg.TEST_STEPS:
        test_dsets, ref_dsets = dm.initialise_test_datasets(prg.THRESHOLD_SIZE)
        test_bbs_dset = ref_dsets['test_bbs_dset']
        test_dsets['test_pos_partOf_dset'] = \
                test_dsets['test_pos_partOf_dset'].sample(test_bbs_dset.bbs)
        test_dsets['test_neg_partOf_dset'] = \
                test_dsets['test_neg_partOf_dset'].sample(test_bbs_dset.bbs)
        test_datasets = [test_dsets, ref_dsets]
    """
    for with_constraints in prg.INCLUDE_CONSTRAINTS:
        log.info('=====Include Contraints: {}====='.format(with_constraints))
        if with_constraints:
            label = '_wc'
        else:
            label = '_nc'
        ### Initialise Tensor Network
        log.info('====Preparing Logic Tensor Network config====')
        if prg.PARAM_SEARCH:
            max_score = 0
            for params in prg.PARAMS:
                hyperparams = {
                        'lr': prg.LR,
                        'regularisation': params[0],
                        'alpha': params[1],
                        'epsilon': params[2]
                        }
                label_conf = label + '_reg{}_alpha{}_eps{}'.format(params[0],
                        params[1], params[2])
                log.info('==Running PARAM Search. Params are: ==')
                log.info('={}='.format(hyperparams))
                LTN, optimizer = setup_LTN(ref_dsets['all_class_types'],
                    ref_dsets['types_in_train'], hyperparams, ontology_dset)
                ## Training
                score = train(experiment_dsets, ontology_dset, ref_dsets, LTN,
                        optimizer, with_constraints, model_label=label_conf)
                if score > max_score:
                    max_score = score
                    max_label = label_conf
            log.info('==Best performing model:\t{}=='.format(max_label))
        else:
            log.info('==Note Running PARAM Search. Params are: ==')
            log.info('={}='.format(prg.HYPERPARAMS))
            LTN, optimizer = setup_LTN(ref_dsets['all_class_types'],
                ref_dsets['types_in_train'], prg.HYPERPARAMS, ontology_dset)
            ## Training
            train(experiment_dsets, ontology_dset, ref_dsets, LTN,
                    optimizer, with_constraints, model_label=label)

    log.info('======TRAINING COMPLETE======')

def setup_LTN(all_class_types, types_in_train, hyperparams,
        ontology_dset):
    import src.LTN_setup as LTN_setup
    from src.logic_tensor_network import LogicTensorNetwork
    ## Predicates and Literals
    log.debug('===Preparing predicate and literal config===')
    """
    Literals:
        - isOfType{type} for each type class
        - isPartOf

    Literals initialise their underlying predicates. Each predicate is e.g. isOfType_Dog...
    They have their dataset instances assigned to them so it is easy to reference and sample -
    Scratch that, need to be able to assign train or test data as necessary... so better
    to set data on each loop?
    """
    predicate_config, literal_config = LTN_setup.setup_literals_and_predicates(
            all_class_types, types_in_train)
    ## Clauses
    log.debug('===Preparing clause config===')
    """
    Clauses:
        - One clause for each isOfType{type7
        - One clause for each partOf(x, y)
		- Further ontology clauses
    """
    ### Standard clauses
    clause_config = LTN_setup.setup_standard_clauses(all_class_types,
            types_in_train)
    ### partOf Ontology Constraints
    if ontology_dset is not None:
        wholes_for = ontology_dset.wholes_for_part
        parts_for = ontology_dset.parts_for_whole
        clause_config += LTN_setup.setup_ontology_clauses(types_in_train,
                wholes_for, parts_for)  # TODO check this will add two outputs

    ## Initialise the LTN with config
    LTN = LogicTensorNetwork(predicate_config, literal_config, clause_config)

    ## optimizer
    log.info('====Initialising Optimizer====')
    # weight_decay performs |omega| norm on parameters. Donadello inputs this
    # explicitly as a "smooth" function output
    optimizer = optim.RMSprop(LTN.parameters(), lr=hyperparams['lr'], alpha=hyperparams['alpha'],
            eps=hyperparams['epsilon'], weight_decay=hyperparams['regularisation'])
    #optimizer = optim.Adam(LTN.parameters(), lr=0.01, eps=prg.EPSILON, weight_decay=prg.REGULARISATION)
    #optimizer = optim.Adam(LTN.parameters(), lr=prg.LR, weight_decay=prg.REGULARISATION)

    return LTN, optimizer


def train(experiment_dsets, ontology_dset, ref_dsets, LTN, optimizer, with_constraints,
        model_label=''):
    import src.loss as Loss
    import src.test as model_test

    # Run torch in train mode - all tensors will default to requires_grad=True
    torch.set_grad_enabled(True)

    # Extract relevant data
    train_bbs_dset = ref_dsets['train_bbs_dset']
    train_pics_dset = ref_dsets['train_pics_dset']
    types = ref_dsets['types_in_train']
    ### Network Initialisation
    log.info('====Initialising Logic Tensor Network with config====')
    log.info('=====TRAINING Phase=====')
    # Feed data - specify datasets for each literal
    # output clauses
    # Apply the loss
    test_performance = 0.
    for i in range(prg.NUM_EPOCHS):
        if i % prg.DATA_RESAMPLE_STEPS == 0:
            log.debug('===Setting up data subsets===')
            ## Obtain data subsets
            # Step sample datasets by their batch sizes
            iteration_dsets = {}
            for k, dset in experiment_dsets.items():
                dset_sample = dset.sample(train_bbs_dset.bbs,
                        prg.BATCH_SIZES[dset.label], inference=False)
                if dset.label in ['isOfType', 'neg_isOfType']:
                    for k, v in dset_sample.items():
                        if len(v) > 0:
                            # Point of assigning dataset to literal
                            iteration_dsets[dset.label+'_'+k] = v
                else:
                    if len(dset_sample) > 0:
                        # Point of assigning dataset to literal
                        iteration_dsets[dset.label] = dset_sample

            clause_dsets = {}
            # TODO: This is annoyingly hacky - found that these clauses were getting
            # defined here and then again in ontology clauses. This cause .backward()
            # to pick up on the inplace op that it wasn't happy with.
            #ontology_clause_names = ['Clause_irrefl_partOf', 'Clause_asym_partOf']
            for clause_name, clause in LTN.clauses.items():
                if all(x not in clause_name for x in ['Clause_partOf', 'Clause_neg_partOf',
                    'Clause_isOfType_','Clause_neg_isOfType_']):
                    continue
                # If we have data for the literal, assign.
                clause_dsets[clause_name] = {}
                for lit in clause.literals:
                    if lit.domain in iteration_dsets:
                        clause_dsets[clause_name][lit.label] = \
                                iteration_dsets[lit.domain]
                    else:
                        # Insufficient data for clause
                        clause_dsets.pop(clause_name)
                        log.debug('==Not enough data for "{}" -- popping=='.format(
                            clause_name))
                        continue
            # Ontology clauses need specific data and override standard literal
            # input
            ontology_clause_dsets = {}
            if with_constraints:
                irrefl_dset = ontology_dset.sample_irreflexive(
                        train_bbs_dset.bbs, prg.BATCH_SIZES['axioms'])
                asym_dset = ontology_dset.sample_asymmetric(
                        train_bbs_dset.bbs, prg.BATCH_SIZES['axioms'])
                part_wholes, parts, wholes, generic_objs = ontology_dset.sample_combos(
                        train_pics_dset.data_pairs, types, prg.BATCH_SIZES['axioms']
                        )

                # Two literals in clause, need same num inputs
                assert asym_dset[0].size(0) == asym_dset[1].size(0)
                ontology_clause_dsets.update({
                    'Clause_irrefl_partOf': {
                        'Literal_neg_partOf': irrefl_dset
                        },
                    'Clause_asym_partOf': {
                        'Literal_partOf': asym_dset[0],
                        'Literal_neg_partOf': asym_dset[1]
                        }
                    })
                # TODO - have to clone everything otherwise in-place op error occurs
                # Need to check if there is a better way. PyTorch gets confused
                # as the same input is used on different computation graphs, so gradients
                # compete - even though input doesn't need gradient...
                for t1, t2 in itertools.combinations(types, 2):
                    ontology_clause_dsets.update({
                        'Clause_'+t1+'_isNot_'+t2: {
                            'Literal_neg_isOfType_'+t1: generic_objs.clone(),
                            'Literal_neg_isOfType_'+t2: generic_objs.clone()
                            }
                        })
                for whole in ontology_dset.wholes_for_part.keys():
                    whole_clause = {
                            'Clause_parts_of_'+whole: {
                                'Literal_neg_isOfType_'+whole:wholes[whole].clone(),
                                'Literal_neg_partOf': part_wholes[whole].clone(),
                                }
                            }
                    for part in ontology_dset.parts_for_whole[whole]:
                        whole_clause['Clause_parts_of_'+whole].update({
                            'Literal_isOfType_'+part: parts[part].clone()
                            })
                    ontology_clause_dsets.update(whole_clause)
                for part in ontology_dset.parts_for_whole.keys():
                    part_clause = {
                            'Clause_wholes_of_'+part: {
                                'Literal_neg_isOfType_'+part:parts[part].clone(),
                                'Literal_neg_partOf': part_wholes[part].clone(),
                                }
                            }
                    for whole in ontology_dset.wholes_for_part[part]:
                        part_clause['Clause_wholes_of_'+part].update({
                            'Literal_isOfType_'+whole: wholes[whole].clone()
                            })
                    ontology_clause_dsets.update(part_clause)

            # Init all cuda related stuffs
            if prg.use_cuda:
                torch.cuda.synchronize()


        # Train LTN on Knowledge Base
        t0 = time.perf_counter()

        clause_grounding = LTN(
                clause_dsets,
                ontology_clause_dsets,
                aggregator=prg.CLAUSE_AGGREGATOR,
                inference=False,
                verbose=prg.VERBOSE
                )
        t1a = time.perf_counter()
        # Optimise
        # Define loss and then step optimiser
        loss = Loss.maximise_grounding_for_aggregator(
                clause_grounding, aggregator=prg.CLAUSE_AGGREGATOR)

        if loss == -0.:
            import pdb
            pdb.set_trace()
            clause_grounding = LTN(
                clause_dsets,
                ontology_clause_dsets,
                aggregator=prg.CLAUSE_AGGREGATOR,
                inference=False,
                verbose=prg.VERBOSE,
                test=True
                )
        #log.debug('=Loss:\t{}='.format(loss))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        t1b = time.perf_counter()
        # All print statements after timing so they don't distort values
        #log.debug('=f - Time elapsed: {}='.format(t1a - t0))
        #log.debug('=f+b - Time elapsed: {}='.format(t1b - t0))
        if i % 20 == 0:
            log.debug('======Iteration:\t{}======'.format(i))
            log.debug('==Score:\t{}=='.format(torch.mean(clause_grounding)))  # Only work for reduce op.
        #grad_counter = 0
        #for param in LTN.parameters():
        #    if param.grad is not None:
        #        if torch.sum(param.grad) != 0:
        #            grad_counter += torch.sum(param.grad)
        #log.debug('=Net Grad:\t{}='.format(grad_counter))
        #if i % 99 == 0:
        #    import pdb
        #    pdb.set_trace()
        #log.debug('==Predicates with gradient:\t{}'.format(grad_counter)) # 4 params per pred
        #log.debug('==Num Predicates:\t{}'.format(len(set(list(clause_dsets.keys())))))
        #del loss, clause_grounding
        #log.debug('==Total Loss: {}=='.format(loss))
        if prg.TEST_MODE and i % prg.TEST_STEPS == (prg.TEST_STEPS - 1):
            # Run progress tests
            log.info('====TESTING: Iteration {}===='.format(i))
            results = model_test.run_test(model=LTN)
            log.debug('====Resetting aggregation and requires_grad for Clauses in Model====')
            ### Run in training mode
            torch.set_grad_enabled(True)
            for clause in LTN.clauses.values():
                clause.aggr_type = prg.AGGREGATOR
            partOf_precs = []
            partOf_recs = []
            for thresh, res in results['partOf'].items():
                partOf_precs.append(res['precision'])
                partOf_recs.append(res['recall'])
            partOf_precs = np.array(partOf_precs)
            partOf_recs = np.array(partOf_recs)

            partOf_recall = partOf_recs
            idx_rec_srt = np.argsort(partOf_recall)  # needed so trapeze integration doesn't screw up
            partOf_precision = partOf_precs
            partOf_auc = np.trapz(np.array(partOf_precision)[idx_rec_srt], x=np.array(partOf_recall)[idx_rec_srt])
            log.info('===Test Performance - PartOf:\t{}==='.format(partOf_auc))
            type_precs = []
            type_recs = []
            for thresh, res in results['isOfType'].items():
                type_precs.append(res['precision'])
                type_recs.append(res['recall'])
            type_precs = np.array(type_precs)
            type_recs = np.array(type_recs)

            type_recall = type_recs
            idx_rec_srt = np.argsort(type_recall)  # needed so trapeze integration doesn't screw up
            type_precision = type_precs
            type_auc = np.trapz(np.array(type_precision)[idx_rec_srt], x=np.array(type_recall)[idx_rec_srt])
            log.info('===Test Performance - isOfType:\t{}==='.format(type_auc))
            log.info('===Test Performance - Aggregate:\t{}==='.format(partOf_auc + type_auc))
            if (type_auc + partOf_auc) < (1-0.05)*test_performance:
                # Early Stopping - don't save worse model
                log.info('===Early Stopping===')
                break
            else:
                if (type_auc + partOf_auc) > test_performance:
                    test_performance = type_auc + partOf_auc
            save_model(LTN, model_label, optimizer, i)

    log.debug('=====Training Complete=====')
    save_model(LTN, model_label, optimizer, 'FINAL')
    return test_performance


if __name__ == '__main__':
    # Setup logger
    args = {}
    args['gen_log_dir'] = True  # create new log directory
    args['log_dir'] = os.path.join(os.getcwd(), 'data/trial')
    args['debug_mode'] = True
    args['prefix'] = prg.PREFIX
    log = Log.setup_log_with_args(args)
    Log.save_config(prg.__dict__)
    # Run Experiment
    run(log)

