"""Manage data storage related stuff e.g. checkpointing and data capture"""
import os

import torch


def save_checkpoint(params, basepath='checkpoints/', filename='checkpoint.pth.tar',
        full_model=False):
    dirpath = basepath

    if not os.path.exists(dirpath):
        os.makedirs(dirpath)

    filepath = os.path.join(dirpath, filename)

    if not full_model:
        checkpoint = {
                'model': params['model'],
                'optimizer': params['optimizer'],
                'epoch': params['epoch'],
                }
        torch.save(checkpoint, filepath)
    else:
        torch.save(params['full_model'], filepath)



def load_checkpoint(basepath='checkpoints/', filename='checkpoint.pth.tar',
        full_model=False):
    filepath = os.path.join(basepath, filename)

    if not full_model:
        checkpoint = torch.load(filepath)

        data = {
                'model': checkpoint['model'],
                'optimizer': checkpoint['optimizer'],
                'epoch': checkpoint['epoch'],
                }

        return data
    else:
        return torch.load(filepath)  # loads the actual model object
