"""
PyTorch implementation of a Logic Tensor Network, using torch ParameterList.
General idea:
    - Predicates, literals and clauses have their config passed in
    - Module __init__ creates the pieces, as dictionaries with labels
    - forward pass can take in a map and input and then compute the components
        from the clause dictionaries. E.g. for given clause, grab literals and
        tie the computation together. The dynamic graph may allow gradients to
        work.
"""
import time

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from src.nn_helper import CustomParameterList
import src.config as prg

from src.log import Log; log = Log.get_logger(__name__)

class LogicTensorNetwork(nn.Module):

    def __init__(self, predicate_conf, literal_conf, clause_conf):
        super(LogicTensorNetwork, self).__init__()
        self.predicates = self.init_predicates(predicate_conf)
        self.literals = self.init_literals(literal_conf)
        self.clauses = self.init_clauses(clause_conf)

        # add all the submodules to the module list
        # they are index 1 of the underlying param tuples of (label, submodule)
        modules = [
                param[1] for clause_label, clause in self.clauses.items()
                for param in clause.parameters
                ]
        names = [
                param[0] for clause_label, clause in self.clauses.items()
                for param in clause.parameters
                ]
        self.modules = CustomParameterList(modules, names)

    def init_predicates(self, conf):
        """Init predicates"""
        predicates = {}
        for pred_conf in conf:
            predicate= self.__class__.Predicate(
                    pred_conf['grounding_dim'],
                    pred_conf['k_depth'],
                    pred_conf['arity'],
                    pred_conf['label']
                    )
            predicates[pred_conf['label']] = predicate
        return predicates

    def init_literals(self, conf):
        """Init literals"""
        literals = {}
        for lit_conf in conf:
            literal = self.__class__.Literal(
                    lit_conf['polarity'],
                    self.predicates[lit_conf['predicate']],
                    lit_conf['domain'],
                    lit_conf['label']
                    )
            literals[lit_conf['label']] = literal
        return literals

    def init_clauses(self, conf):
        """Define and initialise clauses here"""
        clauses = {}
        for clause_conf in conf:
            clause = self.__class__.Clause(
                    [self.literals[lit] for lit in clause_conf['literals']],
                    clause_conf['label'],
                    disjunction_type=clause_conf['disjunction_type'],
                    aggregation_type=clause_conf['aggregation_type'],
                    weight=clause_conf['weight']
                    )
            clauses[clause_conf['label']] = clause

        return clauses


    def forward(self, clause_dsets={}, ont_clause_dsets={}, aggregator='keep_all',
                inference=False, verbose=False, test=False):
        """
        Input:
            - computation map (clause to run)
            - inputs
        Output:
            - Clause grounding computation
        """
        outputs = []
        for clause_name in clause_dsets.keys():
            clause = self.clauses[clause_name]
            if verbose:
                log.debug('===Training Clause: {}==='.format(clause.label))
            # 1) Pass clause's input dictionary as input, which contains the data
            #    per clause literal, with the literal label as key.
            # 2) Compute clause grounding by triggering underlying machinery.
            # 3) Output is a batch column vector, via unsqueeze(1). Append to list
            #    of outputs
            clause_grounding = clause.compute(clause_dsets[clause_name], test=test)
            #log.debug('==Clause:\t{}\t Score:\t{}'.format(clause.label, clause_grounding))
            outputs.append(clause_grounding)
        # Ontology clauses
        for clause_name in ont_clause_dsets.keys():
            clause = self.clauses[clause_name]
            if verbose:
                log.debug('===Training Ontology Clause: {}==='.format(clause.label))
            outputs.append(clause.compute(
               ont_clause_dsets[clause.label]))
        if verbose:
            log.debug('===Aggregating Clauses with {}==='.format(aggregator))
        # Concatenate clauses into big column vector. All clauses have same
        # output expectations (high score), so no need to know which clause is
        # which (unless we want to have that insight in investigation phase)
        if outputs[0].dim() == 0:
            # Case where we are outputting scalar from batch clause aggregation
            clauses_value_tensor = torch.stack(outputs)
        else:
            clauses_value_tensor = torch.cat(outputs, dim=0)
        if aggregator == 'keep_all':
            # TODO This may be sloppy, but batch sizes vary and I want to keep
            # every output, rather than reducing down to a minimum in all batches.
            # The minimum does give the worst performing clause, but wouldn't
            # gradients only then be processed on that clause? What a waste?
            return clauses_value_tensor
        elif aggregator == 'minimum':
            output = torch.min(clauses_value_tensor) # reduce min
        elif aggregator == 'weighted_mean':
            weights_tensor = torch.tensor(
                    [cl.weight for cl in self.clauses], requires_grad=False)
            output = torch.div(
                    torch.sum(
                        torch.mul(weights_tensor, clauses_value_tensor)
                        ),
                    torch.sum(weights_tensor)
                    )
        elif aggregator == 'hmean':
            output = clauses_value_tensor.size(0) / torch.sum(
                    torch.reciprocal(clauses_value_tensor))
        elif aggregator == 'mean':
            output = torch.mean(clauses_value_tensor)
        else:
            raise ValueError('aggregator "{}" not recognized'.format(aggregator))
        #t1 = time.perf_counter()
        #log.debug('=Clause aggregation time: {}='.format(t1-t0))
        return output


    class Literal(object):
        def __init__(self, polarity, predicate, domain, label, tnorm='luk'):
            self.predicate = predicate
            self.polarity = polarity
            self.parameters = predicate.parameters
            self.domain = domain
            self.label = label
            self.tnorm = tnorm

        def compute(self, input_):
            if self.polarity:
                return self.predicate.compute(input_)
            else:
                # Negation: not-P = 1 - |P|
                pos = self.predicate.compute(input_)
                if self.tnorm in ['luk', 'yager2']:
                    negation = torch.ones(pos.size(0), 1, requires_grad=False).sub(pos)
                elif self.tnorm in ['product', 'goedel']:
                    negation = torch.eq(pos, 0.).type(torch.tensor)
                else:
                    raise ValueError('tnorm type "{}" not recognized'.format(self.tnorm))
                return negation

    class Predicate(object):
        """Predicate computation object, as in NTN
        Each call of 'compute' takes in input embeddings vectors and computes the,
        in this implementation, NTN equation.
        """
        def __init__(self, grounding_dim, k_depth, arity, label):
            """
            Args:
                arity: predicates arity, used to assert correct number of arguments
                    during computation
            """
            """
            Note:
                - we are using stacked_grounding_dim as, contrary to NTN, LTN
                paper stacks the predicate arguments such that the same stacked
                vector is on either side of the W matrix. The V operation is unchanged
            """
            # TODO initialise parameters as needed e.g. Gaussian
            self.arity = arity
            self.label = label
            self.k_depth = k_depth

            stacked_grounding_dim = grounding_dim * arity
            #r = 1/np.sqrt(2 * stacked_grounding_dim * 10)  # from NTN
            self.V = nn.Parameter(
                    #torch.tensor(k_depth, stacked_grounding_dim))
                    torch.empty(stacked_grounding_dim, k_depth))
            self.V.data.normal_()

            self.W = nn.Parameter(
                    torch.empty(
                        stacked_grounding_dim, stacked_grounding_dim, k_depth)
                        #k_depth, stacked_grounding_dim, stacked_grounding_dim)
                    )
            self.W.data.normal_()

            self.B = nn.Parameter(
                    #torch.tensor(k_depth)
                    torch.neg(torch.ones(k_depth))
                    )
            #self.B.data.uniform_(-r, r)

            self.U = nn.Parameter(
                    #torch.tensor(k_depth, 1)
                    torch.ones(k_depth, 1)
                    )
            #self.U.data.uniform_(-r, r)

            # TODO don't think I need to do this
            self.tanh = nn.Tanh()
            self.sigmoid = nn.Sigmoid()

            self.parameters = [
                    (label+'_W', self.W),
                    (label+'_V', self.V),
                    (label+'_B', self.B),
                    (label+'_U', self.U)
                    ]

        def compute(self, input_):
            """
            Compute predicate grounding for input_
            Args:
                input_: column vector, with each arg stacked. This is how LTNs
                perform the predicate grounding, whereas NTN would have one argument
                on each side of the W matrix in a e1, R, e2 triplet. LTNs however
                open up to arbitrary numbers of arguments since all of their
                embeddings.
            """
            t0 = time.perf_counter()
            stacked_inputs = input_
            t0_xwx = time.perf_counter()
            batch_h = torch.einsum('bi,ijk,bj->bk',
                    (stacked_inputs, self.W, stacked_inputs))
            t1_xwx = time.perf_counter()
            mx_plus_b = torch.matmul(
                    stacked_inputs, self.V) + self.B  # Broadcast on self.B
            t1_mxplusb = time.perf_counter()
            non_linear = self.tanh(batch_h + mx_plus_b)
            t1_non_linear = time.perf_counter()
            output = self.sigmoid(
                    torch.matmul(non_linear, self.U))
            t1 = time.perf_counter()
            return output



    class Clause:

        def __init__(self, literals, label, disjunction_type='luk',
                aggregation_type='hmean', weight=1.0):
            self.literals = literals
            self.label = label
            self.disjunct_type = disjunction_type
            self.aggr_type = aggregation_type
            self.weight = weight
            self.predicates = set([lit.predicate for lit in literals])
            self.parameters = [
                    param for lit in self.literals for param in lit.parameters
                    ]

        def compute(self, inputs, test=False):
            # Ground the literals
            outputs = []
            for i, lit in enumerate(self.literals):
                # Each input is passed to its corresponding literal
                outputs.append(lit.compute(inputs[lit.label]))
            # Compute the disjunction of literals
            return self.__class__.disjunction_of_literals(outputs,
                    self.disjunct_type, self.aggr_type, test)

        @classmethod
        def disjunction_of_literals(cls, literal_outputs, tnorm='luk',
                aggregator='hmean', test=False):
            #t0 = time.perf_counter()
            torch_lit_outputs = torch.cat(literal_outputs)
            if tnorm == 'luk':
                tnorm_out = torch.min(
                        torch.ones(1, requires_grad=False),
                        torch.sum(torch_lit_outputs, dim=1)
                        )
            else:
                raise ValueError('Unhandled type value for type: %s', tnorm)
            # TODO why would we hmean here? I think for batch process - mean over batch
            if aggregator == 'hmean':
                if test:
                    import pdb
                    pdb.set_trace()
                output = tnorm_out.size(0) / torch.sum(torch.reciprocal(tnorm_out))
                #t1 = time.perf_counter()
                #log.debug('=Clause disjunction time: {}='.format(t1-t0))
            elif aggregator == 'mean':
                output = torch.mean(tnorm_out)
            elif aggregator == 'keep_all':
                output = tnorm_out
            else:
                raise ValueError('Unhandled aggregation for type: %s', aggregator)
            return output




    # Unused
    class Function(object):
        def __init__(self, grounding_dim, arity, label, value=None):
            self.arty = arity
            self.label = label
            self.domains = domains
            self.ranges = ranges
            self.rows = np.prod([d.rows for d in domains])
            self.domain_columns = np.sum([d.columns for d in domains])
            self.columns = np.sum([d.columns for d in ranges])
            self.value = value

            if self.value:
                self.parameters = []
            else:
                self.M = nn.Parameter(
                        torch.tensor(
                            grounding_dim, grounding_dim, arity).normal_()
                        )
                self.B = nn.Parameter(
                        torch.tensor(
                            1, grounding_dim
                            ).normal_()
                        )
                self.parameters = [
                        (label+'_M', self.M),
                        (label+'_B', self.B)
                        ]

        def compute(self, inputs):
            """
            Args:
                inputs: set of column vectors
            """
            if self.value:
                return self.value
            else:
                # assert self.arity == len(inputs)
                # TODO check below computation
                # Column stack vectors into 2D matrix
                X = torch.cat([input_ for input_ in inputs], dim=0)
                # Linear transformation on domain vectors (constants)
                # i domain embedding size, k domains (or vectors) to be transformed
                # they are then summed to have one final net transform vector
                MX = einsum('iik,ik->i', self.M, X)

                return MX + self.B

    # Unused
    class Constant(object):
        def __init__(self, label, value=None, domain=None):
            self.rows = 1  # Row Vector
            self.label = label
            if value:  # Just a value constant
                # Constant value Tensor
                # Note: Value can be a list. In which case tensor is a row vector
                self.tensor = torch.tensor([value])
                self.parameters = []
                self.columns = len(value)
            else:
                self.columns = domain.columns
                # Guassian init Tensor of shape [1, domain_rows]
                self.C = torch.tensor(1, domain.rows).normal_(mean=1, std=1)
                self.tensor = torch.div(
                        torch.matmul(torch.abs(self.C), domain.tensor),
                        torch.sum(torch.abs(self.C))
                        )
                self.parameters = [self.C]

