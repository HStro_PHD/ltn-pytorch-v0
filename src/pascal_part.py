"""Create Dataset(s) for PASCALPART data
Use relevant paths in config to prepare datasets from the pascalpart data.

This whole module should exist as part of the PASCALPART dataset, stored together.
It then becomes a PyTorch interface to the data.

In the future I want Datasets to specify the LTN structure, since we can assign
domains to the literals.
"""
import numpy as np
import random
import csv

import torch
use_cuda = torch.cuda.is_available()
if use_cuda:
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    # Makes sure device=None falls back to cuda device
device = torch.device('cuda' if use_cuda else 'cpu')


# PASCALPART data related config

# Data paths
root_data_dir = "/media/data/datasets/pascal_part/processed/"
DATA_SMALL_TRAIN_DIR = root_data_dir+"data/small_training/"
DATA_TRAIN_DIR = root_data_dir+'data/training/'
DATA_TEST_DIR = root_data_dir+"data/testing/"
TRAIN_DATA_PATH = DATA_TRAIN_DIR + "bbFeaturesTrain.csv"
TEST_DATA_PATH = DATA_TEST_DIR + "bbFeaturesTest.csv"

ONTOLOGY_PATH = root_data_dir+"data/pascalPartOntology.csv"
TYPE_LIST_PATH = root_data_dir+"data/classes.csv"

# Bounding box types data (e.g. CAT, DOG...)
TRAIN_TYPES_PATH = DATA_TRAIN_DIR+"bbUnaryPredicatesTrain.txt"
TEST_TYPES_PATH = DATA_TEST_DIR+"bbUnaryPredicatesTest.txt"
# Bounding box partOf data - what bounding boxes a bounding box part of... (I think)
TRAIN_PARTOF_PATH = DATA_TRAIN_DIR+"bbPartOfTrain.txt"
TEST_PARTOF_PATH = DATA_TEST_DIR+"bbPartOfTest.txt"




def compute_inclusion_ratio(bb1,bb2):
    """Compute Inclusion Ratio (IR)"""
    bb1_area = (bb1[-2] - bb1[-4]) * (bb1[-1] - bb1[-3])
    bb2_area = (bb2[-2] - bb2[-4]) * (bb2[-1] - bb2[-3])
    w_intersec = max(0,min([bb1[-2], bb2[-2]]) - max([bb1[-4], bb2[-4]]))
    h_intersec = max(0,min([bb1[-1], bb2[-1]]) - max([bb1[-3], bb2[-3]]))
    bb_area_intersection = w_intersec * h_intersec
    return [float(bb_area_intersection)/bb1_area, float(bb_area_intersection)/bb2_area]

def concat_gnding_for_partOf(data):
    instance = np.concatenate(
                [
                    data[0],
                    data[1],
                    compute_inclusion_ratio(
                        data[0][-4:], data[1][-4:]
                        ) # so feature set includes the inclusion ratio
                ]
            )
    return instance



def get_class_types(path):
    """Get the list of class type names
    Note: Removing background class as it doesn't seem to be used anywhere
    """
    with open(path) as f:
        types = f.read().splitlines()
    return types

def get_types_for_bbs(path, types, max_rows=0):
    """
    Read type indexes from file and then extract the textual type name
    from types list.
    """
    if max_rows:
        idx_of_type_of_data = np.genfromtxt(path,
            delimiter=",",max_rows=max_rows,dtype=np.int)
    else:
        idx_of_type_of_data = np.genfromtxt(path,
            delimiter=",",dtype=np.int)
    # Note: -1 is because the background class was removed from classes.csv.
    # As it was the first line, all indexes move back by 1
    # The other -1 is that idx_of_type list is 1 indexed, not zero like types list
    return np.array([types[idx-1-1] for idx in idx_of_type_of_data])

def get_data(path, max_rows=0):
    """Read data features file
    Structure:
        {picture index}, {type score from FRCNN}, {top right/bottom left x,y}
    """
    if max_rows:
        data = np.genfromtxt(path,delimiter=",",max_rows=max_rows)
    else:
        data = np.genfromtxt(path,delimiter=",")
    return data

def get_pics_idxs(data):
    """Get picture indexes for each train data feature set
    The zeroth feature is the picture index
    """
    result = {}
    for i in range(len(data)):
        if data[i][0] in result:
            result[data[i][0]].append(i)
        else:
            result[data[i][0]] = [i]
    return result

def get_partOf_parent_for_bbs(path, max_rows=None, filter_=[]):
    """
    Get the parent of a bb in partOf relation. -1 indicates no parent.
    Returns the index of the parent.
    """
    parents = []
    with open(path, 'r') as f:
        for i, line in enumerate(f):
            if max_rows:
                if len(parents) >= max_rows: break
            if i not in filter_:
                parents.append(line)
    return np.array(parents, dtype=np.int)


#In the future can do weak ref where each dataset is added to a parent dataset
#class dictionary, so they can be easily referenced in line with respective literals
#class Dataset(objects):
#    Datasets = {}
#    def __init__(self):
#        pass


class BoundingBoxDataset:
    """Dataset of bounding boxes in PASCALPART data. Will have one per dataset
    e.g. Test, Train, Validation...
    """
    def __init__(self, data, types, wholes, data_subset=None, filter_threshold=0.,
            label='bounding_box_features'):
        """Load in the data for the dataset. Each bounding box data point can
        be filtered against a threshold size.

        Each bounding box has:
            Compulsory:
                - an index (corresponds to row in features data file)
                - a feature vector
                - a picture index
                - an associated type
                - a partOf parent (if None then it is a whole)
        """
        self.label = label
        self.bbs = {}
        for i, features in enumerate(data):
            # Check if bb is in data subset - continue if not
            if data_subset is not None and types[i] not in data_subset:
                continue
            # Not sure about these 500s
            if self.bb_is_big_enough(features[-4:], filter_threshold):
                features[4:] /= 500  # Otherwise scale is off. Donadello uses 500
                self.bbs[i] = {
                        'pic_idx': int(features[0]),
                        'features': features[1:],
                        'type': types[i],
                        'parent_whole': wholes[i] if wholes[i] >= 0 else None,
                    }
        print('Deleted {} of {} bbs for threshold {}'.format((len(data)-len(self.bbs)),
            len(data), filter_threshold))

    @staticmethod
    def bb_is_big_enough(bb, threshold):
        """Check if bb is large enough"""
        is_valid_x = (bb[2] - bb[0]) >= threshold
        is_valid_y = (bb[3] - bb[1]) >= threshold
        if is_valid_x and is_valid_y:
            return True
        return False

    def __len__(self):
        return len(self.bbs)


class PartOfDataset:
    # 64 for types and coords. +1 for inclusion ratio, *2 for binary predicate
    num_features = (64 + 1) * 2

class PositivePartOfDataset(PartOfDataset):

    def __init__(self, bbs, label='partOf'):
        """Input bb dataset. Create dataset of bbs with parent whole
        Note: partOf(x, y) read as "is x part of y?"
        """
        self.label = label
        self.partOf_pairs = []
        for k, v in bbs.items():
            # Make sure the parent_whole exists and that the associated bb
            #made it through bb thresholding.
            if v['parent_whole'] is not None and v['parent_whole'] in bbs.keys():
                self.partOf_pairs.append((v['parent_whole'], k))


    def sample(self, bbs, num_samples=None, inference=False):
        if num_samples:
            pairs = random.sample(self.partOf_pairs, num_samples)
        else:
            pairs = self.partOf_pairs
        feature_pairs = []
        for pair in pairs:
            i = pair[0]
            j = pair[1]
            feature_pairs.append(
                        torch.tensor(
                            concat_gnding_for_partOf(
                                (bbs[i]['features'], bbs[j]['features'])),
                            dtype=torch.float32,
                            requires_grad=False,
                            device=device
                            ).unsqueeze(1)
                        )
        return torch.cat(feature_pairs, dim=1).t().to(device)

    def __len__(self):
        return len(self.partOf_pairs)

    def __getitem__(self, idx):
        return self.partOf_pairs[idx]

class NegativePartOfDataset(PartOfDataset):

    def __init__(self, pics, bbs, label='neg_partOf'):
        """For each picture, create set of negative partOf examples.
        Note:
            - the check is that y is not the parent whole of x
            - Only bounding boxes within the same picture are combined.
            - Usually one would just keep one of (a, b) and (b, a), but
              the partOf predicate is asymmetric, so we keep both.

        Can use DataLoader to pull batches
        """
        self.label = label
        self.partOf_pairs = []
        for pic, contained_bbs in pics.items():
            pairs = [
                    (x, y)
                    for x in contained_bbs
                    for y in contained_bbs
                    ]
            for pair in pairs:
                # make sure x is not part of y
                if pair[0] != bbs[pair[1]]['parent_whole']:
                    self.partOf_pairs.append(pair)

    def sample(self, bbs, num_samples=None, inference=False):
        if num_samples:
            pairs = random.sample(self.partOf_pairs, num_samples)
        else:
            pairs = self.partOf_pairs

        feature_pairs = [
                    torch.tensor(
                        concat_gnding_for_partOf(
                            (bbs[i]['features'], bbs[j]['features'])),
                        dtype=torch.float32,
                        requires_grad=False,
                        device=device
                        ).unsqueeze(1)
                for (i, j) in pairs
                ]
        return torch.cat(feature_pairs, dim=1).t().to(device)

    def __len__(self):
        return len(self.partOf_pairs)

    def __getitem(self, idx):
        return self.partOf_pairs[idx]


class PicturesDataset:
    """Contains picture indexes with inner bbs indexes"""

    def __init__(self, bbs, for_constraints=False, label='pictures'):
        """
        Set up a dictionary based Dataset with pic_idx: bb_idxs.
        bb_dataset is a dictionary with index: {parameters dictionary}
        """
        self.label = label
        self.pics = {}
        for k, v in bbs.items():
            if v['pic_idx'] in self.pics:
                self.pics[v['pic_idx']].append(k)
            else:
                self.pics[v['pic_idx']] = [k]
        if for_constraints:
            print('Getting picture bb pairs')
            self.data_pairs = self.get_data_pairs(bbs)

    def get_data_pairs(self, bbs):
        feature_pairs = []
        for p in self.pics:
            feature_pairs += [
                        torch.tensor(
                            concat_gnding_for_partOf(
                                (bbs[i]['features'], bbs[j]['features'])),
                            dtype=torch.float32,
                            requires_grad=False,
                            device=device
                            ).unsqueeze(1)
                        for i in self.pics[p] for j in self.pics[p]]
        return torch.cat(feature_pairs, dim=1).t().to(device)


    def sample(self, num_samples=None):
        if num_samples:
            return random.sample(self.pics, num_samples)
        else:
            return self.partOf_pairs

    def __len__(self):
        return len(self.pics)

    def __getitem(self, key):
        return self.partOf_pairs[key]

class PositiveIsOfTypeDataset:
    """Dictionary based Dataset that provides type keys with values as the
    bounding boxes (indexes) of that type.
    """

    def __init__(self, bbs, label='isOfType'):
        self.label = label
        self.types = {}
        for k, v in bbs.items():
            if v['type'] in self.types:
                self.types[v['type']].append(k)
            else:
                self.types[v['type']] = [k]
        for k, v in self.types.items():
            if len(v) == 0:
                import pdb
                pdb.set_trace()


    def sample(self, bbs, num_samples=None, inference=False):
        if num_samples:
            sample = {}
            for key in self.types.keys():
                sample[key] = []
            for type_ in sample.keys():
                samples = np.random.choice(self.types[type_], size=num_samples,
                        replace=(num_samples >= len(self.types[type_])))
                for bb_idx in samples:
                    sample[type_].append(
                                torch.tensor(
                                    bbs[bb_idx]['features'],
                                    dtype=torch.float32,
                                    requires_grad=False,
                                    device=device
                                    ).unsqueeze(1)
                                )
            # output concat of groundings, so a batch
            for type_ in sample.keys():
                sample[type_] = torch.cat(sample[type_], dim=1).t().to(device)
            return sample
        else:
            #TODO need to convert dictionary to include features, not indexes
            sample = {}
            for key in self.types.keys():
                sample[key] = []
            for type_ in sample.keys():
                samples = self.types[type_]
                for bb_idx in samples:
                    sample[type_].append(
                            torch.tensor(
                                bbs[bb_idx]['features'],
                                dtype=torch.float32,
                                requires_grad=False,
                                device=device
                                ).unsqueeze(1)
                            )
            # output concat of groundings, so a batch
            for type_ in sample.keys():
                sample[type_] = torch.cat(sample[type_], dim=1).t().to(device)
            return sample


    def __len__(self):
        return len(self.types)

class NegativeIsOfTypeDataset:
    def __init__(self, bbs, list_of_types, label='neg_isOfType'):
        self.label = label
        self.types = {}
        for type_ in list_of_types:
            self.types[type_] = []
        for k, v in bbs.items():
            for type_ in list_of_types:
                if v['type'] != type_:
                    self.types[type_].append(k)
        for k, v in self.types.items():
            if len(v) == 0:
                import pdb
                pdb.set_trace()

    def sample(self, bbs, num_samples=None, inference=False):
        if num_samples:
            sample = {}
            for key in self.types.keys():
                sample[key] = []
            for type_ in sample.keys():
                samples = np.random.choice(self.types[type_], size=num_samples,
                        replace=(num_samples >= len(self.types[type_])))
                for bb_idx in samples:
                    sample[type_].append(
                            torch.tensor(
                                bbs[bb_idx]['features'],
                                dtype=torch.float32,
                                requires_grad=False,
                                device=device
                                ).unsqueeze(1)
                            )
            # output concat of groundings, so a batch
            for type_ in sample.keys():
                sample[type_] = torch.cat(sample[type_], dim=1).t().to(device)
            return sample
        else:
            # convert indexes to associated features
            sample = {}
            for type_, bbs_idxs in self.types.items():
                sample[type_] = []
                for bb_idx in bbs_idxs:
                    sample[type_].append(
                            torch.tensor(
                                bbs[bb_idx]['features'],
                                dtype=torch.float32,
                                requires_grad=False,
                                device=device
                                ).unsqueeze(1)
                            )
                sample[type_] = torch.cat(sample[type_], dim=1).t().to(device)
            return sample


    def __len__(self):
        return len(self.types)

class OntologyDataset:
    """Dataset to parse and store ontology from file"""

    def __init__(self, path, types, label='axioms'):
        self.label = label
        parts_for_whole = {}
        wholes_for_part = {}
        with open(path, 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                parts_for_whole[row[0]] = row[1:]
                for part in row[1:]:
                    if part in wholes_for_part:
                        wholes_for_part[part].append(row[0])
                    else:
                        wholes_for_part[part] = [row[0]]
        for whole in parts_for_whole:
            wholes_for_part[whole] = []
        for part in wholes_for_part:
            if part not in parts_for_whole:
                parts_for_whole[part] = []
        self.parts_for_whole = {}
        self.wholes_for_part = {}
        for t in types:
            if t not in parts_for_whole: parts_for_whole[t] = []
            if t not in wholes_for_part: wholes_for_part[t] = []
            self.parts_for_whole[t] = [p for p in parts_for_whole[t] if p in types]
            self.wholes_for_part[t] = [w for w in wholes_for_part[t] if w in types]


    def sample_irreflexive(self, bbs, num_samples=None, inference=False):
        if num_samples == None:
            num_samples = len(bbs)
        rnd_bbs = np.random.choice(list(bbs.keys()), size=num_samples, replace=False)
        # Irreflexivity
        irreflexivity_sample = [
                        torch.tensor(
                            concat_gnding_for_partOf(
                                (bbs[k]['features'], bbs[k]['features'])
                                ),
                            dtype=torch.float32,
                            requires_grad=False,
                            device=device
                            ).unsqueeze(1)
                for k in rnd_bbs
                ]

        irreflexivity_sample = torch.cat(irreflexivity_sample, dim=1).t()
        return irreflexivity_sample.to(device)

    def sample_asymmetric(self, bbs, num_samples=None, inference=False):
        if num_samples == None:
            num_samples = len(bbs)
        keys = list(bbs.keys())
        rnd_bbs = np.random.choice(keys, size=num_samples, replace=False)
        part_whole = []
        whole_part = []
        while True:
            for i in rnd_bbs:
                if bbs[i]['parent_whole'] is not None:
                    try:
                        p_w = torch.tensor(
                                    concat_gnding_for_partOf(
                                        (
                                            bbs[i]['features'],
                                            bbs[bbs[i]['parent_whole']]['features']
                                            )
                                        ),
                                    dtype=torch.float32,
                                    requires_grad=False,
                                    device=device
                                    ).unsqueeze(1)
                        w_p = torch.tensor(
                                concat_gnding_for_partOf(
                                    (
                                        bbs[bbs[i]['parent_whole']]['features'],
                                        bbs[i]['features']
                                        )
                                    ),
                                dtype=torch.float32,
                                requires_grad=False,
                                device=device
                                ).unsqueeze(1)
                    except KeyError:
                        continue
                    else:
                        part_whole.append(p_w)
                        whole_part.append(w_p)
            # Case where num_samples is smaller than number of bbs
            if len(part_whole) == num_samples:
                break
            else:
                rnd_bbs = np.random.choice(keys, replace=False,
                        size=(num_samples-len(part_whole)))
        part_whole = torch.cat(part_whole, dim=1).t()
        whole_part = torch.cat(whole_part, dim=1).t()

        return whole_part.to(device), part_whole.to(device)

    def sample_combos(self, data_pairs, types, num_samples):
        num_features = int((data_pairs[0].shape[0] / 2) - 1)  # TODO check
        rand_part_wholes_idxs = np.random.choice(
                data_pairs.shape[0], num_samples, replace=False)
        rand_part_wholes = data_pairs[rand_part_wholes_idxs, :]
        generic_objects = rand_part_wholes[:, :num_features]
        part_wholes = {}
        parts = {}
        wholes = {}
        for t in types:
            part_wholes[t] = rand_part_wholes
            parts[t] = part_wholes[t][:, :num_features]
            wholes[t] = part_wholes[t][:, num_features:-2]  # last two are containments
        return part_wholes, parts, wholes, generic_objects






def initialise_train_datasets(bb_size_threshold, data_subset=None, with_ontology=False):
    # Bounding Box Features
    train_bb_features = get_data(TRAIN_DATA_PATH)

    # Data class types - all are in text
    all_class_types = get_class_types(TYPE_LIST_PATH)
    # remove background class as not used
    # No bounding box is given background type. Only way to assign to background
    # is based on agent outputting "unknown" LTN isOfType range on all types
    # for given bb. However, this is a heuristic. Better to just leave out background.
    # Note: We must then reindex the data_types file, by bring each index down by 1.
    # This is done in hte get_types_for_bbs function
    all_class_types.pop(0)

    train_types = get_types_for_bbs(TRAIN_TYPES_PATH, all_class_types)
    # filter out the class_types that aren't in the data. This is used to create LTN predicates
    types_in_train = [t for t in all_class_types if t in train_types]
    if data_subset:
        types_in_train = [t for t in data_subset if t in types_in_train]

    # For each training data instance, these are the 'whole' parts (by index) that
    # train data is 'partOf'. We need to first delete the corresponding entries based
    # on bounding box size filtering result.
    train_partOf_parent_idxs = get_partOf_parent_for_bbs(
            TRAIN_PARTOF_PATH
            )

    # Picture indexes
    train_pics_kv_idxs = get_pics_idxs(train_bb_features)

    ## prepare datasets
    # Train
    train_bbs_dset = BoundingBoxDataset(train_bb_features, train_types,
            train_partOf_parent_idxs, data_subset, bb_size_threshold)
    train_pics_dset = PicturesDataset(train_bbs_dset.bbs, with_ontology)
    train_pos_partOf_dset = PositivePartOfDataset(train_bbs_dset.bbs)
    train_neg_partOf_dset = NegativePartOfDataset(train_pics_dset.pics,
            train_bbs_dset.bbs)
    train_pos_isOfType_dset = PositiveIsOfTypeDataset(train_bbs_dset.bbs)
    train_neg_isOfType_dset = NegativeIsOfTypeDataset(train_bbs_dset.bbs, types_in_train)

    # Ontology
    if with_ontology:
        ontology_dset = OntologyDataset(ONTOLOGY_PATH, types_in_train)
    else:
        ontology_dset = None

    dsets = {
            'train_pos_partOf_dset': train_pos_partOf_dset,
            'train_neg_partOf_dset': train_neg_partOf_dset,
            'train_pos_isOfType_dset': train_pos_isOfType_dset,
            'train_neg_isOfType_dset': train_neg_isOfType_dset
            }
    reference_dsets = {
            'train_bbs_dset': train_bbs_dset,
            'train_pics_dset': train_pics_dset,
            'all_class_types': all_class_types,
            'types_in_train': types_in_train
            }
    return dsets, ontology_dset, reference_dsets # ontology treated differently

def initialise_test_datasets(bb_size_threshold, data_subset=None):

    test_bb_features = get_data(TEST_DATA_PATH)

    test_pics_kv_idxs = get_pics_idxs(test_bb_features)

    # Data class types - all are in text
    all_class_types = get_class_types(TYPE_LIST_PATH)
    # remove background class as not used
    # No bounding box is given background type. Only way to assign to background
    # is based on agent outputting "unknown" LTN isOfType range on all types
    # for given bb. However, this is a heuristic. Better to just leave out background.
    # Note: We must then reindex the data_types file, by bringing each index down by 1.
    # This is done in the get_types_for_bbs function
    all_class_types.pop(0)

    test_types = get_types_for_bbs(TEST_TYPES_PATH, all_class_types)
    types_in_test = [t for t in all_class_types if t in test_types]
    if data_subset:
        types_in_test = [t for t in data_subset if t in types_in_test]


    test_partOf_parent_idxs = get_partOf_parent_for_bbs(
            TEST_PARTOF_PATH
            )

    # Test
    test_bbs_dset = BoundingBoxDataset(test_bb_features, test_types,
            test_partOf_parent_idxs, data_subset, bb_size_threshold)
    test_pics_dset = PicturesDataset(test_bbs_dset.bbs)
    test_pos_partOf_dset = PositivePartOfDataset(test_bbs_dset.bbs)
    test_neg_partOf_dset = NegativePartOfDataset(test_pics_dset.pics,
            test_bbs_dset.bbs)
    test_pos_isOfType_dset = PositiveIsOfTypeDataset(test_bbs_dset.bbs)
    test_neg_isOfType_dset = NegativeIsOfTypeDataset(test_bbs_dset.bbs, all_class_types)

    dsets = {
            'test_pos_partOf_dset': test_pos_partOf_dset,
            'test_neg_partOf_dset': test_neg_partOf_dset,
            'test_pos_isOfType_dset': test_pos_isOfType_dset,
            'test_neg_isOfType_dset': test_neg_isOfType_dset
            }

    reference_dsets = {
            'test_bbs_dset': test_bbs_dset,
            #'test_pics_dset': test_pics_dset,
            'all_class_types': all_class_types,
            'types_in_test': types_in_test
            }

    return dsets, reference_dsets
