import logging
import sys
import os
import cson

class Log(object):
    filename = None
    log_dir = None
    level = None
    root_logger = None

    @classmethod
    def setup(cls, log_dir, debug=True, gen_dir=True):
        cls.log_dir = log_dir + '/'
        filename = cls.log_dir + 'log.txt'
        cls.filename = filename
        cls.checkpoint_dir = cls.log_dir + 'checkpoints/'
        cls.debug = debug
        cls.level = logging.DEBUG if debug else logging.INFO
        file_formatter = logging.Formatter('%(asctime)s - %(name)-20s - %(levelname)-8s : %(message)s')
        if gen_dir:
            file_handler = logging.FileHandler(filename)
        else:
            # May be appending to a prexisting log file
            file_handler = logging.FileHandler(filename, mode='a')
        file_handler.setFormatter(file_formatter)

        stream_formatter = logging.Formatter('%(asctime)s [%(name)-20s] [%(levelname)-8s] : %(message)s',
                datefmt='%H:%M:%S')
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(stream_formatter)

        cls.root_logger = logging.getLogger()
        cls.root_logger.setLevel(cls.level)
        cls.root_logger.addHandler(file_handler)
        cls.root_logger.addHandler(stream_handler)


    @classmethod
    def get_logger(cls, name):
        if cls.root_logger is None:
            cls.root_logger = logging.getLogger()
            cls.root_logger.warning('No root logger set! Falling back')

        return cls.root_logger#.getChild(name)

    @classmethod
    def gen_log_dir(cls, base_dir, prefix=''):
        # Create a folder to hold results
        import time
        time_str = time.strftime("%d-%m-%Y-%H-%M-%S", time.gmtime())
        if prefix == '' or prefix is None:
            base_log_dir = time_str
        else:
            base_log_dir = '{}_{}'.format(prefix, time_str)

        log_dir = base_dir + '/' + base_log_dir
        os.makedirs(log_dir, exist_ok=True)

        link_path = base_dir + '/last'

        try:
            # Clean up old softlink
            os.remove(link_path)
        except OSError as ex:
            pass # All G

        os.symlink(base_log_dir, link_path)

        return log_dir

    @classmethod
    def setup_log_with_args(cls, args):
        if args['gen_log_dir']:
            args['log_dir'] = cls.gen_log_dir(args['log_dir'], args['prefix'])
        # If not gen_log_dir, expect log file to already be present

        cls.setup(args['log_dir'], args['debug_mode'], args['gen_log_dir'])
        log = cls.get_logger(__name__)

        if 'LD_PRELOAD' in os.environ and 'tcmalloc' in os.environ['LD_PRELOAD']:
            log.debug('Using tcmalloc!')
        else:
            log.warning('NOT using tcmalloc')

        return log

    @classmethod
    def save_config(cls, config_dict, prefix_dir=''):
        def get_name(obj):
            try:
                return obj.__name__
            except AttributeError:
                return type(obj)


        file_name = 'config'
        file_dir = cls.log_dir + prefix_dir

        os.makedirs(file_dir, exist_ok=True)  # make directory to store config

        file_path = file_dir + '{}.cson'.format(file_name)
        with open(file_path, 'w') as f:
            cson.dump(dict(config_dict), f, sort_keys=True, indent=4, default=get_name)

    @classmethod
    def save_git_version(cls, file_name='gitversion', file_path='./'):
        import subprocess
        import os
        try:
            cwd = os.getcwd()
            os.chdir(file_path)
            sha = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])
            os.chdir(cwd)
            save_path = os.path.join(cls.log_dir, file_name)
            with open(save_path, 'w') as f:
                f.write(sha.decode())
            return True
        except:
            return False
