"""Evaluate performance of LTN"""

import numpy as np
import torch
import random

import src.config as prg

import src.pascal_part as dm

from src.log import Log

log = None

# For Confustion matrix and rec-prec plotting
import matplotlib.pyplot as plt
import numpy as np
import itertools



def plot_prec_rec_curve(precisions, recalls, labels):
    """For each precision and recall (coming from different evaluation models)
    produce precision-recall curve"""
    import matplotlib.pyplot as plt

    fig = plt.figure(figsize=(10.0, 8.0))

    for i in range(len(labels)):
        label = labels[i]
        prec = precisions[i]
        recall = recallss[i]

        # Make sure plot is in order..?
        rec_srt_idx = np.argsort(rec)



def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.figure(figsize=(20,20))
    plt.imshow(cm, interpolation='nearest', cmap=cmap, aspect='auto')
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j]),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def confusion_matrix_for_baseline(thresholds,with_partof_axioms=False):
    confusion_matrix_for_types = {}
    confusion_matrix_for_pof = {}
    for th in thresholds:
        confusion_matrix_for_types[th] = np.matrix([[0.0] * len(selected_types)] * len(selected_types))
        for bb_idx in range(len(test_data)):
            for st_idx in range(len(selected_types)):
                st_feature_of_bb_idx = test_data[bb_idx][1+idxs_for_selected_types[st_idx]]
                if st_feature_of_bb_idx >= th:
                    confusion_matrix_for_types[th][st_idx,np.where(selected_types == types_of_test_data[bb_idx])[0][0]]+= 1

        confusion_matrix_for_pof[th] = np.matrix([[0.0,0.0],[0.0,0.0]])

        wholes_of_part={}
        if with_partof_axioms:
            _, wholes_of_part = get_part_whole_ontology()

        for bb_pair_idx in range(len(pairs_of_test_data)):
            if partof_baseline_test(bb_pair_idx, wholes_of_part, with_partof_axioms=with_partof_axioms):
                if partOF_of_pairs_of_test_data[bb_pair_idx]:
                    confusion_matrix_for_pof[th][0,0] +=1
                else:
                    confusion_matrix_for_pof[th][0,1] +=1
            else:
                if partOF_of_pairs_of_test_data[bb_pair_idx]:
                    confusion_matrix_for_pof[th][1,0] += 1
                else:
                    confusion_matrix_for_pof[th][1,1] += 1

    return confusion_matrix_for_types, confusion_matrix_for_pof


def isOfType_quick_eval(model, test_bbs, all_class_types,
        decision_threshold=0.5, label=''):
    # get all isOfType scores for each test datapoint. Then take argmax to see
    # which type wins. Can then score performance based on true type.
    neum_types = len(all_class_types)
    test_bb_features = []
    test_bb_types = []
    num_correct = 0
    max_prediction = 0
    for k, bb in test_bbs.items():
        test_bb_features.append(bb['features'])
        test_bb_types.append(bb['type'])
    for t in all_class_types:
        #log.debug('==Evaluation isOfType {}=='.format(t))
        if t == 'background':
            #log.debug('=Skipping {}='.format(t))
            continue
        type_features = [
                test_bb_features[i]
                for i, x in enumerate(test_bb_types) if x == t
                ]
        clause = 'Clause_isOfType_'+t
        literal = 'Literal_isOfType_'+t
        dset = {clause: {literal: type_features}}
        predictions = model(dset, inference=True)
        iter_max_pred = torch.max(predictions)
        max_prediction = iter_max_pred if iter_max_pred > max_prediction else max_prediction
        right_answers = torch.sum(torch.ge(predictions, decision_threshold))
        num_correct += right_answers
    log.debug('=Max isOfType prediction {}='.format(max_prediction))
    # Assuming that every test_bb_feature is used in predictions so = total_predictions
    log.debug('=Accuracy isOfType overall: {} ='.format(num_correct/len(test_bb_features)))


def isOfType_predicate_eval(model, test_bbs, class_types,
        decision_thresholds=[0.5], label='', fast_run=False, verbose=False):
    # get all isOfType scores for each test datapoint. Then take argmax to see
    # which type wins. Can then score performance based on true type.
    num_types = len(class_types)
    predictions = []
    test_bb_features = []
    test_bb_types = []
    for k, bb in test_bbs.items():
        test_bb_features.append(
                torch.tensor(
                    bb['features'],
                    dtype=torch.float32,
                    requires_grad=False,
                    device=prg.DEVICE
                    ).unsqueeze(1),
                )
        test_bb_types.append(bb['type'])
    if fast_run:
        # sub sample
        bb_idxs_samples = random.sample(list(enumerate(test_bb_features)), 800)
        bb_features_subset = []
        bb_types_subset = []
        for idx, val in bb_idxs_samples:
            bb_features_subset.append(val)
            bb_types_subset.append(test_bb_types[idx])
        test_bb_features = bb_features_subset
        test_bb_types = bb_types_subset
    test_bb_features = torch.cat(test_bb_features, dim=1).t()
    if verbose:
        log.debug('=isOfType tests consist of {} bbs='.format(test_bb_features.shape[0]))
    # Agent can say that any isOfType output > threshold means a positive
    # prediction for the type.
    # Can implement a case where multi guess isn't allow, but this is TODO.
    # Instead, in recall, one must get the true isOfType_t count another way
    # as multiguess breaks confusion matrix. Usually they only consider one guess
    # per instance, meaning it is either wrong or right in prediciton, not both.
    type_count_array = np.zeros((len(class_types),), dtype=np.float32)
    for i, t in enumerate(class_types):
        type_count_array[i] = test_bb_types.count(t)
        if type_count_array[i] == 0:
            log.debug('=No data for class:\t{}='.format(t))
            #import pdb
            #pdb.set_trace()

    for t in class_types:
        # Every bbox is evaluated under every isOfType clause. Then each
        # row of final output is each bbs score for the type, with rows aligned
        # to the class_types rows i.e. first row is first element of classes.
        if verbose:
            log.debug('==Evaluation isOfType {}=='.format(t))
        clause = 'Clause_isOfType_'+t
        literal = 'Literal_isOfType_'+t
        dset = {clause: {literal: test_bb_features}}
        #log.debug('=Testing isOfType_{}'.format(t))
        predictions.append(model(dset, inference=True, aggregator='keep_all'))
    if predictions[0].dim() == 1:
        # Case where we are outputting scalar from batch clause aggregation
        predictions = torch.stack(predictions, dim=1)
    else:
        predictions = torch.cat(predictions, dim=1)
    predictions = predictions.t().cpu()
    predictions = predictions.data.numpy()
    if verbose:
        log.debug('=Max isOfType prediction {}='.format(np.max(predictions)))

    results = {}
    precisions = []
    recalls = []
    for decision_threshold in decision_thresholds:
        ## instantiate confusion matrix
        cm_shape = (num_types, num_types)
        confusion_matrix = np.zeros(cm_shape)
        # TODO verify below evaluation will work
        # Apply threshold mask to outputs
        #if allow_multi_guess:  # case where multi guess allowed (breaks cm)
        # Each column is a bb
        mask = (predictions >= decision_threshold).astype(int)
        for bb_output_idx in range(mask.shape[1]):
            votes = mask[:, bb_output_idx]
            actual_type_idx = class_types.index(test_bb_types[bb_output_idx])
            confusion_matrix[actual_type_idx, :] += votes

        ## Get stats from CM
        num_predicted_true = confusion_matrix.sum(axis=1)
        num_actually_true = type_count_array
        #confusion_matrix.sum(axis=1) if cm was not multi choice

        precision = np.true_divide(np.diag(confusion_matrix), num_predicted_true)
        recall = np.true_divide(np.diag(confusion_matrix), num_actually_true)
        # TODO remove this - dealing with NaNs brutally
        precision = np.nan_to_num(precision)
        recall = np.nan_to_num(recall)
        f1 = np.true_divide(2*precision*recall, precision + recall)
        f1 = np.nan_to_num(f1)

        mean_precision = np.mean(precision)
        mean_recall = np.mean(recall)
        mean_f1 = np.mean(f1)

        Accuracy = np.nan_to_num(np.sum(np.diag(confusion_matrix))/np.sum(confusion_matrix))
        if verbose:
            log.debug('=isOfType Results:\tDT\t{}\tPrecision:\t{}\tRecall:\t{}\tF1:\t{}\tAcc:\t{}='.format(
            decision_threshold, mean_precision, mean_recall, mean_f1, Accuracy
            ))
        results["{:.2f}".format(decision_threshold)] = {
                'precision': mean_precision,
                'recall': mean_recall,
                'f1': mean_f1,
                'confusion_matrix': confusion_matrix
                }
        precisions.append(mean_precision)
        recalls.append(mean_recall)
    return results

def partOf_predicate_eval(model, pos_data, neg_data, decision_thresholds=[0.5],
        label='', fast_run=False, verbose=False):
    if verbose:
        log.debug('=Number partOf instances:\t{}=\n=Number neg_partOf instances:\t{}='.format(
            pos_data.shape[0], neg_data.shape[0]
            ))
    # Evaluation on true partOf
    clause = 'Clause_partOf'
    literal = 'Literal_partOf'
    dset = {clause: {literal: pos_data}}
    true_predictions = model(dset, aggregator='keep_all').cpu()
    if verbose:
        log.debug('=Max partOf score: {}='.format(torch.max(true_predictions)))

    # Evaluation on partOf where it should score low
    if fast_run:
        neg_data = random.sample(neg_data, len(pos_data))
    neg_dset = {clause: {literal: neg_data}}
    neg_predictions = model(neg_dset, inference=True, aggregator='keep_all').cpu()

    results = {}
    precisions = []
    recalls = []
    for i, decision_threshold in enumerate(decision_thresholds):
        tp_byte_tensor = torch.ge(true_predictions, decision_threshold)
        tp = torch.sum(tp_byte_tensor.int()).numpy() # ge comparison with broadcast
        fn = pos_data.size(0) - tp  # the rest are fn
        fp_byte_tensor = torch.ge(neg_predictions, decision_threshold)
        fp = torch.sum(fp_byte_tensor.int()).numpy() # ge comparison with broadcast
        tn = neg_data.size(0) - fp  # the rest are tn

        # get stats
        precision = tp/(fp + tp)
        if np.isnan(precision):
            precision = precisions[i-1]
        recall = tp/(tp + fn)
        f1 = np.nan_to_num(2*precision*recall / (precision + recall))

        # Get scalars
        confusion_matrix = np.array([[tp, fn], [fp, tn]])
        #cm_normalised = confusion_matrix.astype('float') / confusion_matrix.sum(axis=1)[:, None]

        Accuracy = np.nan_to_num(np.sum(np.diag(confusion_matrix))/np.sum(confusion_matrix))

        if verbose:
            log.debug('=partOf Results:\tDT\t{}\tPrecision:\t{}\tRecall:\t{}\tF1:\t{}\tAcc:\t{}='.format(
            decision_threshold, precision, recall, f1, Accuracy
            ))
        results["{:.2f}".format(decision_threshold)] = {
                'precision': precision,
                'recall': recall,
                'f1': f1,
                'confusion_matrix': confusion_matrix
                }
        precisions.append(precision)
        recalls.append(recall)
    return results



def run_test(datasets=None, model=None, model_path=None, model_label='', fast_run=False):
    ### Run in inference mode
    torch.set_grad_enabled(False)
    ###
    if model_path is not None:
        # Setup logger
        import os
        args = {}
        args['gen_log_dir'] = False  # create new log directory
        # Log goes in parent of checkpoint
        args['log_dir'] = os.path.join(model_path, '..')
        args['debug_mode'] = True
        Log.setup_log_with_args(args)
        # Load full model
        import src.checkpoint_manager as c_m
        model = c_m.load_checkpoint(
                basepath=model_path,
                filename='checkpoint_full{}.pth.tar'.format(model_label),
                full_model=True)

    # Grab the prepped logger. If running solo tests then we'll be appending
    # to the train run's log file.
    global log
    log = Log.get_logger(__name__)

    log.debug('====Switching aggregation off on Clauses in Model====')
    for clause in model.clauses.values():
        clause.aggr_type = 'keep_all'

    log.debug('====Running Test====')
    # Grab data
    log.debug('===Get new datasets: {}==='.format(datasets is None))
    if datasets is None:
        # initialise datasets and get all data as sample
        dsets, ref_dsets = dm.initialise_test_datasets(
                prg.THRESHOLD_SIZE, prg.DATA_SUBSET)
        test_bbs_dset = ref_dsets['test_bbs_dset']
        pos_partOf = dsets['test_pos_partOf_dset'].sample(test_bbs_dset.bbs)
        neg_partOf = dsets['test_neg_partOf_dset'].sample(test_bbs_dset.bbs)
                #num_samples=len(pos_partOf)) # Get as many as we have positive
    else:
        # Already sampled
        dsets, ref_dsets = datasets
        pos_partOf = dsets['test_pos_partOf_dset']
        neg_partOf = dsets['test_neg_partOf_dset']

    test_bbs_dset = ref_dsets['test_bbs_dset']
    all_class_types = ref_dsets['all_class_types']
    types_in_test = ref_dsets['types_in_test']

    if fast_run:
        # Only get basic accuracies
        isOfType_quick_eval(model, test_bbs_dset.bbs, all_class_types, prg.DECISION_THRESHOLD)
        partOf_predicate_eval(model, pos_partOf, neg_partOf, prg.DECISION_THRESHOLD,
                fast_run=True)
    else:
        results = {
                'isOfType': {},
                'partOf': {}
                }
        results['isOfType'] = isOfType_predicate_eval(
                model, test_bbs_dset.bbs, types_in_test, prg.TEST_THRESHOLDS, fast_run=False)
        results['partOf'] = partOf_predicate_eval(model,
                pos_partOf, neg_partOf, prg.TEST_THRESHOLDS, fast_run=False)
        results['classes'] = types_in_test # for jupyter plotting
        return results

