"""Experiment Config paramters"""

import torch

import numpy as np

## Setup torch
# Cuda tensors
use_cuda = torch.cuda.is_available()
if use_cuda:
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    # This makes sure device=None falls back to cuda device on all torch.tensor
    # init
DEVICE = torch.device('cuda' if use_cuda else 'cpu')
#FloatTensor = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
#LongTensor = torch.cuda.LongTensor if use_cuda else torch.LongTensor
#ByteTensor = torch.cuda.ByteTensor if use_cuda else torch.ByteTensor
#Tensor = FloatTensor

PREFIX = 'run'

## Data interface paths
## Data processing
THRESHOLD_SIZE = 6.  # 6px dim Threshold to use for bounding box size filtering
## Datasets
#DATA_SUBSET = None  # Set to None if for full data run
DATA_SUBSET = ['bottle','body','cap','pottedplant','plant','pot','tvmonitor','screen','chair','sofa','diningtable'] # Set to None if using all data
#DATA_SUBSET = ['aeroplane','artifact_wing','body','engine','stern','wheel','bicycle','chain_wheel','handlebar','headlight','saddle','bus','bodywork','door','license_plate','mirror','window','car','motorbike','train','coach','locomotive','boat']
#DATA_SUBSET = ['person','arm','ear','ebrow','foot','hair','hand','mouth','nose','eye','head','leg','neck','torso','cat','tail','bird','animal_wing','beak','sheep','horn','muzzle','cow','dog','horse','hoof']


# Batch size
BATCH_SIZES = {
        'partOf': 250,
        'neg_partOf': 250,
        'isOfType': 250,
        'neg_isOfType': 250,
        'axioms': 1000
        }

INCLUDE_CONSTRAINTS = [True]
## Experiment
# Runner
NUM_EPOCHS = 4000
DATA_RESAMPLE_STEPS = 100
TEST_MODE = True
TEST_STEPS = 500  # Datasets updated each 100 steps, so run just before
VERBOSE = False
## Model - as per paper
# Grounding features: total of 60 possible isTypeOf types in dataset.
# 4 location coords for top-left and bottom-right (x,y) coords
# In script, some have added feature for inclusion ratio
GROUNDING_DIM = 60 + 4
PARTOF_K_DEPTH = 2  # Paper used this
ISOFTYPE_K_DEPTH = 5
T_NORM = 'luk'
AGGREGATOR = 'mean' # hmean, keep_all  # Batch results aggregator
CLAUSE_AGGREGATOR = 'mean' # hmean, keep_all  # Aggregating clause outputs

# Optimiser values
LR = 1e-2
REGULARISATION = 1e-15 #[10**-i for i in range(8, 17)]
ALPHA = 0.9 #[9*10**-i for i in range(1, 3)]  # This seems to be the decay of TF RMSprop
EPSILON = 1e-10 #[10**-i for i in range(7, 12)] # Matching Tensorflow


## Evaluation Specific
DECISION_THRESHOLD = 0.7
TEST_THRESHOLDS = np.arange(.00, 1.1, .05)


# Param Search
PARAM_SEARCH = False  # if true, grid search over hyperparams (each should be a list)
if PARAM_SEARCH:
    import itertools
    PARAMS = list(itertools.product(
            REGULARISATION,
            ALPHA,
            EPSILON
            ))
else:
    HYPERPARAMS = {
            'lr': LR,
            'regularisation': REGULARISATION,
            'alpha': ALPHA,
            'epsilon': EPSILON
            }
