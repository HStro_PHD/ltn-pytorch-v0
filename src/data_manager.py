"""Deals with accessing all data and preparing Datasets"""

import numpy as np

import src.data_processor as dp
import src.data_loader as dl
import src.config as prg

from src.log import Log; log=Log.get_logger(__name__)


## Loading and preparing data for either Evaluation or Train scripts
def prepare_train_script_data():
    train_bounding_box_groundings = dl.get_data(prg.TRAIN_DATA_PATH)
    test_bounding_box_groundings = dl.get_data(prg.TEST_DATA_PATH)
    all_class_types = dl.get_class_typenames(prg.TYPE_LIST_PATH)
    # isOfType type indexes for each training data line
    train_data_types = dl.get_data_types(prg.TRAIN_TYPES_PATH, all_class_types)
    # filter out the class_types that aren't in the data
    types_in_data = [t for t in all_class_types if t in train_data_types]

    # For each training data instance, these are the 'whole' parts (by index) that
    # train data is 'partOf'. We need to first delete the corresponding entries based
    # on bounding box size filtering result.
    train_data_partof_parent_idxs = dl.get_partof_parent_for_bbs(
            prg.TRAIN_PARTOF_PATH,
            len(train_bounding_box_groundings)
            )

    train_pics_kv_idxs = dl.get_pics_idxs(train_bounding_box_groundings)
    test_pics_kv_idxs = dl.get_pics_idxs(test_bounding_box_groundings)

    # Ontology
    parts_for_whole, wholes_for_part = dl.get_part_whole_ontology(prg.ONTOLOGY_PATH)

    output_dict = dict(
            train_bbs=train_bounding_box_groundings,
            test_bbs=test_bounding_box_groundings,
            all_class_types=all_class_types,
            types_in_data=types_in_data,
            type_for_train_data=train_data_types,
            train_partOf_whole_idxs=train_data_partof_parent_idxs,
            train_pic_bbs_dict=train_pics_kv_idxs,
            test_pic_bbs_dict=test_pics_kv_idxs,
            parts_for_whole_dict=parts_for_whole,
            wholes_for_part_dict=wholes_for_part
            )

    return output_dict


def prepare_evaluation_script_data(preload_data=None):
    # preload data would be present if testing during a training run
    if preload_data == None:
        train_bounding_box_groundings = dl.get_data(prg.TRAIN_DATA_PATH)
        test_bounding_box_groundings = dl.get_data(prg.TEST_DATA_PATH)
        all_class_types = dl.get_class_typenames(prg.TYPE_LIST_PATH)
        # isOfType type indexes for each training data line
        train_data_types = dl.get_data_types(prg.TRAIN_TYPES_PATH, all_class_types)
        # filter out the class_types that aren't in the data
        types_in_data = [t for t in all_class_types if t in train_data_types]

        # For each training data instance, these are the 'whole' parts (by index) that
        # train data is 'partOf'. We need to first delete the corresponding entries based
        # on bounding box size filtering result.
        test_data_partof_parent_idxs = dl.get_partof_parent_for_bbs(
                prg.TEST_PARTOF_PATH,
                len(test_bounding_box_groundings)
                )

        test_pics_kv_idxs = dl.get_pics_idxs(test_bounding_box_groundings)

        output_dict = dict(
                train_bbs=train_bounding_box_groundings,
                test_bbs=test_bounding_box_groundings,
                all_class_types=all_class_types,
                types_in_data=types_in_data,
                type_for_train_data=train_data_types,
                test_partOf_whole_idxs=test_data_partof_parent_idxs,
                test_pic_bbs_dict=test_pics_kv_idxs,
                )
    else:
        import copy
        output_dict = copy.deepcopy(preload_data)

    test_data_types = dl.get_data_types(
            prg.TEST_TYPES_PATH, output_dict['all_class_types'])
    output_dict['all_class_types'].remove('background')
    output_dict['type_for_test_data'] = test_data_types

    return output_dict


def prepare_train_datasets(train_bb_features, test_bb_features, partOf_whole_idxs,
        train_pic_bbs_dict, test_pic_bbs_dict, all_class_types, train_data_types):

    pos_partOf_instance_idxs = dp.get_pos_partOf_with_threshold(
            prg.THRESHOLD_SIZE,
            train_bb_features,
            partOf_whole_idxs
            )
    neg_partOf_instance_idxs = dp.get_neg_partOf_with_threshold(
            prg.THRESHOLD_SIZE,
            train_pic_bbs_dict,
            train_bb_features,
            partOf_whole_idxs
            )
    # Potentials
    predicted_pics_part_whole_instance_idxs = dp.get_predicted_partOf_with_threshold(
            prg.THRESHOLD_SIZE,
            test_pic_bbs_dict,
            test_bb_features
            )
    # isOfType positive and negative sample sets
    pos_instance_idxs_for_type, _ = dp.get_pos_neg_isOfType_with_threshold(
            train_data_types,
            all_class_types,
            train_bb_features,
            prg.THRESHOLD_SIZE
            )

    datasets = {
            'partOf': {
                'positive': pos_partOf_instance_idxs,
                'negative': neg_partOf_instance_idxs
                },
            'isOfType': {
                'positive': pos_instance_idxs_for_type
                },
            'predicted': {
                'partOf': {
                    'part-whole': predicted_pics_part_whole_instance_idxs
                    }
            }
        }
    return datasets

def prepare_evaluation_datasets(test_bb_features, partOf_whole_idxs,
        test_pic_bbs_dict, test_data_types, all_class_types):
    pos_partOf_instance_idxs = dp.get_pos_partOf_with_threshold(
            prg.THRESHOLD_SIZE,
            test_bb_features,
            partOf_whole_idxs
            )
    neg_partOf_instance_idxs = dp.get_neg_partOf_with_threshold(
            prg.THRESHOLD_SIZE,
            test_pic_bbs_dict,
            test_bb_features,
            partOf_whole_idxs,
            limit=prg.TEST_NEG_PARTOF_LIMIT
            )
    pos_instance_idxs_for_type, _ = dp.get_pos_neg_isOfType_with_threshold(
            test_data_types,all_class_types, test_bb_features, prg.THRESHOLD_SIZE)

    ## Get groundings ready to pass to LTN model
    # partOf
    pos_partOf_groundings = dp.concat_groundings_for_partOf(test_bb_features,
            pos_partOf_instance_idxs)
    neg_partOf_groundings = dp.concat_groundings_for_partOf(test_bb_features,
            neg_partOf_instance_idxs)

    # isOfType_t
    pos_isOfType_groundings = {}
    for t in all_class_types:
        pos_isOfType_groundings[t] = [
                test_bb_features[idx] for idx in pos_instance_idxs_for_type[t]
                ]
    # All test features, minus the picture index
    test_features = test_bb_features[:, 1:]

    datasets = {
            'all_test_bbs': test_features,
            'partOf' : {
                'positive': pos_partOf_groundings,
                'positive_idxs': pos_partOf_instance_idxs,
                'negative': neg_partOf_groundings,
                'negative_idxs': neg_partOf_instance_idxs
                },
            'isOfType': {
                'positive': pos_isOfType_groundings,
                'positive_idxs': pos_instance_idxs_for_type
                }
            }

    return datasets

#### Train loop functions
def prepare_standard_subsets(train_features, test_features, all_class_types,
    types_in_data, datasets):
    # partOf
    pos_partOf_pairs, neg_partOf_pairs = dp.get_partOf_data_subset(
            train_features,
            datasets['partOf']['positive'],
            datasets['partOf']['negative'],
            prg.POS_PARTOF_DSET_SIZE,
            prg.NEG_PARTOF_DSET_SIZE
            )
    pos_objects_for_types, neg_objects_for_types = dp.get_types_data_subset(
            all_class_types,
            types_in_data,
            train_features,
            datasets['isOfType']['positive'],
            prg.POS_TYPES_DSET_SIZE
            )

    # k: v up datasets for clauses to pick from
    datasets = {
            'partOf': {
                'positive': pos_partOf_pairs,
                'negative': neg_partOf_pairs
                },
            'isOfType': {
                'positive': pos_objects_for_types,
                'negative': neg_objects_for_types
                }
            }

    return datasets

def prepare_ontology_subsets(train_features, test_features, datasets):
    # Potentials
    # get random subset of potential part-whole pairs
    rand_potential_partOf_idxs = dp.get_random_indexes(
            datasets['predicted']['partOf']['part-whole'],
            int(prg.NUM_RANDOM_PREDICTED_PAIRS * prg.BALANCE_PARAM)
            )
    rand_pos_partOf_idxs = dp.get_random_indexes(
            datasets['partOf']['positive'],
            int(prg.NUM_RANDOM_PREDICTED_PAIRS * (1-prg.BALANCE_PARAM))
            )
    potential_part, potential_whole = dp.get_part_and_whole_objects_for_ontology(
            train_features,
            test_features,
            rand_pos_partOf_idxs,
            rand_potential_partOf_idxs
            )
    potential_part_whole = dp.get_potential_part_whole_pairs(
            train_features,
            test_features,
            rand_pos_partOf_idxs,
            rand_potential_partOf_idxs
            )
    potential_whole_part = dp.get_potential_whole_part_pairs(
            train_features,
            test_features,
            rand_pos_partOf_idxs,
            rand_potential_partOf_idxs
            )
    potential_part_whole_equal = dp.get_potential_part_whole_equal(
            test_features,
            num_to_get=prg.NUM_RANDOM_PREDICTED_PAIRS
            )

    # k: v up datasets for clauses to pick from
    datasets = {
        'partOf': {
            'part': potential_part,
            'whole': potential_whole,
            'part_whole': potential_part_whole,
            'whole_part': potential_whole_part,
            'whole_and_part_equal': potential_part_whole_equal
            }
        }

    return datasets

def assign_data_to_literals(all_class_types, types_in_data, datasets):
    literal_dsets = {}
    for t in all_class_types:
        if t in types_in_data:  # TODO clean this up
            literal_dsets['Literal_isOfType_'+t] = datasets['isOfType']['positive'][t]
        literal_dsets['Literal_neg_isOfType_'+t] = datasets['isOfType']['negative'][t]
    literal_dsets['Literal_partOf'] = datasets['partOf']['positive']
    literal_dsets['Literal_neg_partOf'] = datasets['partOf']['negative']

    return literal_dsets

def assign_data_to_basic_ontology_clauses(ontology_dsets):
    # Unwrap dsets to extract from
    pw = ontology_dsets['partOf']['part_whole']
    wp = ontology_dsets['partOf']['whole_part']
    wape = ontology_dsets['partOf']['whole_and_part_equal']
    # Assign to clause literals
    clause_dsets = {}
    clause_dsets['Clause_asym_partOf'] = {}
    clause_dsets['Clause_asym_partOf']['Literal_partOf'] = pw
    clause_dsets['Clause_asym_partOf']['Literal_neg_partOf'] = wp
    # partOf irreflexive
    clause_dsets['Clause_irrefl_partOf'] = {}
    clause_dsets['Clause_irrefl_partOf']['Literal_neg_partOf'] = wape
    return clause_dsets

def assign_data_to_type_constraint_ontology_clauses(parts_for_whole, wholes_for_part,
        ontology_dsets):
    # Unwrap dsets to extract from
    pw = ontology_dsets['partOf']['part_whole']
    wp = ontology_dsets['partOf']['whole_part']
    wape = ontology_dsets['partOf']['whole_and_part_equal']
    p = ontology_dsets['partOf']['part']
    w = ontology_dsets['partOf']['whole']
    # Assign to literals in clauses
    dsets = {}
    # Parts of a type and type not part of anything
    for whole in parts_for_whole:
        dsets['Clause_partsOf_'+whole] = {}
        dsets['Clause_partsOf_'+whole]['Literal_neg_isOfType_'+whole] = w
        dsets['Clause_partsOf_'+whole]['Literal_neg_partOf'] = pw
        for t in parts_for_whole[whole]:
            dsets['Clause_partsOf_'+whole]['Literal_isOfType_'+t] = p
        # To make t not part of anything
        dsets['Clause_'+whole+'_not_part_of_anything'] = {}
        dsets['Clause_'+whole+'_not_part_of_anything']['Literal_neg_isOfType_'+ whole] = w
        dsets['Clause_'+whole+'_not_part_of_anything']['Literal_neg_partOf'] = wp

    for part in wholes_for_part:
        dsets['Clause_wholes_of_'+part] = {}
        dsets['Clause_wholes_of_'+part]['Literal_neg_isOfType_'+part] = p
        dsets['Clause_wholes_of_'+part]['Literal_neg_partOf'] = pw
        for whole in wholes_for_part[part]:
            dsets['Clause_wholes_of_'+part]['Literal_isOfType_'+whole] = w

        # To make t not part of anything
        dsets['Clause_'+part+'_has_no_parts'] = {}
        dsets['Clause_'+part+'_has_no_parts']['Literal_neg_isOfType_'+part] = p
        dsets['Clause_'+part+'_has_no_parts']['Literal_neg_partOf'] = wp

    return dsets


