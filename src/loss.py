"""Logic Tensor Network Losses"""

import torch


def maximise_grounding_for_aggregator(clause_groundings, aggregator):
    # make clause groundings as large as possible, so loss is as low as possible
    #loss = torch.ones_like(
    #        clause_groundings, requires_grad=False).sub(clause_groundings)
    loss = clause_groundings
    if loss.dim() != 0:
        loss = loss.sum()
    return -loss

