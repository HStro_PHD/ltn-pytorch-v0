"""Setup LTN component config"""

import itertools

import src.config as prg



def setup_literals_and_predicates(all_class_types, types_in_data):
    predicate_config = []
    literal_config = []
    ## isOfType_t
    for t in types_in_data:# all_class_types:
        # Predicate
        predicate = {}
        predicate['grounding_dim'] = prg.GROUNDING_DIM
        predicate['k_depth'] = prg.ISOFTYPE_K_DEPTH
        predicate['arity'] = 1
        predicate['label'] = 'Predicate_isOfType_'+t
        predicate_config.append(predicate)
        # Literal
        neg_literal = {}
        neg_literal['label'] = 'Literal_neg_isOfType_'+t
        neg_literal['polarity'] = False
        neg_literal['predicate'] = predicate['label']
        neg_literal['domain'] = 'neg_isOfType_'+t
        literal_config.append(neg_literal)
    #for t in types_in_data:
        literal = {}
        literal['label'] = 'Literal_isOfType_'+t
        literal['polarity'] = True
        literal['predicate'] = 'Predicate_isOfType_'+t
        literal['domain'] = 'isOfType_'+t
        literal_config.append(literal)

    ## partOf(x,y)
    # Predicate
    pred_partOf = {}
    pred_partOf['grounding_dim'] = prg.GROUNDING_DIM + 1  # +1 is Inclusion Ratio
    pred_partOf['k_depth'] = prg.PARTOF_K_DEPTH
    pred_partOf['arity'] = 2
    pred_partOf['label'] = 'Predicate_partOf'
    predicate_config.append(pred_partOf)
    # Literal
    lit_partOf = {}
    lit_partOf['label'] = 'Literal_partOf'
    lit_partOf['polarity'] = True
    lit_partOf['predicate'] = pred_partOf['label']
    lit_partOf['domain'] = 'partOf'
    literal_config.append(lit_partOf)
    lit_neg_partOf = lit_partOf.copy()
    lit_neg_partOf['label'] = 'Literal_neg_partOf'
    lit_neg_partOf['polarity'] = False
    lit_neg_partOf['domain'] = 'neg_partOf'
    literal_config.append(lit_neg_partOf)

    return predicate_config, literal_config



def setup_standard_clauses(all_class_types, types_in_data):
    config_for_clauses = []
    # isOfType_t positive
    for t in types_in_data:
        clause_isOfType_t = {}
        clause_isOfType_t['label'] = 'Clause_isOfType_'+t
        clause_isOfType_t['literals'] = ['Literal_isOfType_'+t]
        clause_isOfType_t['disjunction_type'] = prg.T_NORM
        clause_isOfType_t['aggregation_type'] = prg.AGGREGATOR
        clause_isOfType_t['weight'] = 1.
        config_for_clauses.append(clause_isOfType_t)
    # ¬isOfType_t
    #for t in all_class_types:
        clause_neg_isOfType_t = {}
        clause_neg_isOfType_t['label'] = 'Clause_neg_isOfType_'+t
        clause_neg_isOfType_t['literals'] = ['Literal_neg_isOfType_'+t]
        clause_neg_isOfType_t['disjunction_type'] = prg.T_NORM
        clause_neg_isOfType_t['aggregation_type'] = prg.AGGREGATOR
        clause_neg_isOfType_t['weight'] = 1.
        config_for_clauses.append(clause_neg_isOfType_t)

    ## Positive partOf
    clause_partOf = {}
    clause_partOf['label'] = 'Clause_partOf'
    clause_partOf['literals'] = ['Literal_partOf']
    clause_partOf['disjunction_type'] = prg.T_NORM
    clause_partOf['aggregation_type'] = prg.AGGREGATOR
    clause_partOf['weight'] = 1.
    config_for_clauses.append(clause_partOf)
    ## ¬partOf
    clause_neg_partOf = {}
    clause_neg_partOf['label'] = 'Clause_neg_partOf'
    clause_neg_partOf['literals'] = ['Literal_neg_partOf']
    clause_neg_partOf['disjunction_type'] = prg.T_NORM
    clause_neg_partOf['aggregation_type'] = prg.AGGREGATOR
    clause_neg_partOf['weight'] = 1.
    config_for_clauses.append(clause_neg_partOf)

    return config_for_clauses

def setup_ontology_clauses(types, wholes_for, parts_for):
    basic_constraints = []
    # Asymmetric partOf constraint
    clause_asym_partOf = {}
    clause_asym_partOf['label'] = 'Clause_asym_partOf'
    clause_asym_partOf['literals'] = ['Literal_partOf', 'Literal_neg_partOf']
    clause_asym_partOf['disjunction_type'] = prg.T_NORM
    clause_asym_partOf['aggregation_type'] = prg.AGGREGATOR
    clause_asym_partOf['weight'] = 1.
    basic_constraints.append(clause_asym_partOf)
    # Irreflexivity of partOf - (¬partOf(x, x))
    clause_irrefl_partOf = {}
    clause_irrefl_partOf['label'] = 'Clause_irrefl_partOf'
    clause_irrefl_partOf['literals'] = ['Literal_neg_partOf']
    clause_irrefl_partOf['disjunction_type'] = prg.T_NORM
    clause_irrefl_partOf['aggregation_type'] = prg.AGGREGATOR
    clause_irrefl_partOf['weight'] = 1.
    basic_constraints.append(clause_irrefl_partOf)

    ## More complex constraints
    complex_constraints = []
    # Enforce disjoint types (can't be two types)
    for t1, t2 in itertools.combinations(types, 2):
        if t1 == t2:
            continue
        clause_disj_type = {}
        clause_disj_type['label'] = 'Clause_'+t1+'_isNot_'+t2
        clause_disj_type['literals'] = ['Literal_neg_isOfType_'+t1, 'Literal_neg_isOfType_'+t2]
        clause_disj_type['disjunction_type'] = prg.T_NORM
        clause_disj_type['aggregation_type'] = prg.AGGREGATOR
        clause_disj_type['weight'] = 1.
        complex_constraints.append(clause_disj_type)

    # Enforce parts of whole
    for whole in parts_for.keys():
        clause_parts_of_wholes = {}
        clause_parts_of_wholes['label'] = 'Clause_parts_of_'+whole
        clause_parts_of_wholes['literals'] = [
                'Literal_neg_isOfType_'+whole,
                'Literal_neg_partOf'
                ] + [
                        'Literal_isOfType_'+part for part in parts_for[whole]
                        ]
        clause_parts_of_wholes['disjunction_type'] = prg.T_NORM
        clause_parts_of_wholes['aggregation_type'] = prg.AGGREGATOR
        clause_parts_of_wholes['weight'] = 1.
        complex_constraints.append(clause_parts_of_wholes)

    # Enforce wholes of part
    for part in wholes_for.keys():
        clause_wholes_of_parts = {}
        clause_wholes_of_parts['label'] = 'Clause_wholes_of_'+part
        clause_wholes_of_parts['literals'] = [
                'Literal_neg_isOfType_'+part,
                'Literal_neg_partOf'
                ] + [
                        'Literal_isOfType_'+whole for whole in wholes_for[part]
                        ]
        clause_wholes_of_parts['disjunction_type'] = prg.T_NORM
        clause_wholes_of_parts['aggregation_type'] = prg.AGGREGATOR
        clause_wholes_of_parts['weight'] = 1.
        complex_constraints.append(clause_wholes_of_parts)



    return basic_constraints + complex_constraints

